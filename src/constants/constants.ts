export enum FIGHTER_CONTROLS {
  PlayerOneAttack = "KeyA",
  PlayerOneBlock = "KeyD",
  PlayerTwoAttack = "KeyJ",
  PlayerTwoBlock = "KeyL"
}

export enum FIRST_FIGHTER_COMBINATION {
  KeyQ,
  KeyW,
  KeyE
}

export enum SECOND_FIGHTER_COMBINATION {
  KeyU,
  KeyI,
  KeyO
}