import { createElement } from '../helpers/domHelper';
import { renderArena } from './arena';
import { createFighterPreview } from './fighterPreview';
import { fighterService } from '../services/fightersService';
import { Fighter } from "../../types/index";
// @ts-ignore: Unreachable code error
import versusImg from "../../../resources/versus.png";

export function createFightersSelector(): (e: Event, id: string) => Promise<void> {
  let selectedFighters: Array<Fighter> = [];

  return async (event: Event, fighterId: string): Promise<void> => {
    const fighter: Fighter = await getFighterInfo(fighterId);
    const [playerOne, playerTwo] = selectedFighters;
    const firstFighter = playerOne ? playerOne : fighter;
    const secondFighter = Boolean(playerOne) ? (playerTwo ? playerTwo : fighter ): playerTwo;
    selectedFighters = [firstFighter, secondFighter];
    renderSelectedFighters(selectedFighters);
  };
}

const fightersDetailsMap = new Map();

export async function getFighterInfo(fighterId: string): Promise<Fighter> {
  try {
    if(!fightersDetailsMap.has(fighterId)) {
      const fighterInfo = await fighterService.getFighterDetails(fighterId);
      fightersDetailsMap.set(fighterId, fighterInfo);
    }
    
    return fightersDetailsMap.get(fighterId);
  } catch (error) {
    throw new Error("Failed to select");
  }
}

function renderSelectedFighters(selectedFighters: Array<Fighter>): void {
  const fightersPreview = document.querySelector('.preview-container___root');
  const [playerOne, playerTwo] = selectedFighters;
  const firstPreview = createFighterPreview(playerOne, 'left');
  const secondPreview = createFighterPreview(playerTwo, 'right');
  const versusBlock = createVersusBlock(selectedFighters);

  fightersPreview.innerHTML = '';
  fightersPreview.append(firstPreview, versusBlock, secondPreview);
}

function createVersusBlock(selectedFighters: Array<Fighter>): HTMLElement {
  const canStartFight = selectedFighters.filter(Boolean).length === 2;
  const onClick = () => startFight(selectedFighters);
  const container = createElement({ tagName: 'div', className: 'preview-container___versus-block', });
  const image = createElement({
    tagName: 'img',
    className: 'preview-container___versus-img',
    attributes: { src: versusImg },
  });
  const disabledBtn = canStartFight ? '' : 'disabled';
  const fightBtn = createElement({
    tagName: 'button',
    className: `preview-container___fight-btn ${disabledBtn}`,
  });

  fightBtn.addEventListener('click', onClick, false);
  fightBtn.innerText = 'Fight';
  container.append(image, fightBtn);

  return container;
}

function startFight(selectedFighters: Array<Fighter>): void {
  renderArena(selectedFighters);
}