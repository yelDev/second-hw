import { createElement } from '../helpers/domHelper';
import { createFighterImage } from './fighterPreview';
import { fight } from "./fight";
import { showWinnerModal } from "./modal/winner";
import { Fighter } from "../../types/index";

export function renderArena(selectedFighters: Array<Fighter>) {
  const root = document.getElementById('root');
  const arena = createArena(selectedFighters);
  const [firstFighter, secondFigter] = selectedFighters;
  root.innerHTML = '';
  root.append(arena);


  fight(firstFighter, secondFigter)
    .then((data: Fighter) => showWinnerModal(data))
    .catch(err => console.log(err));
}

function createArena(selectedFighters: Array<Fighter>) {
  const arena = createElement({ tagName: 'div', className: 'arena___root' });
  const [firstFighter, secondFigter] = selectedFighters;
  const healthIndicators = createHealthIndicators(firstFighter, secondFigter);
  const fighters = createFighters(firstFighter, secondFigter);
  
  arena.append(healthIndicators, fighters);
  return arena;
}

function createHealthIndicators(leftFighter: Fighter, rightFighter: Fighter) {
  const healthIndicators = createElement({ tagName: 'div', className: 'arena___fight-status' });
  const versusSign = createElement({ tagName: 'div', className: 'arena___versus-sign' });
  const leftFighterIndicator = createHealthIndicator(leftFighter, 'left');
  const rightFighterIndicator = createHealthIndicator(rightFighter, 'right');

  healthIndicators.append(leftFighterIndicator, versusSign, rightFighterIndicator);
  return healthIndicators;
}

function createHealthIndicator(fighter: Fighter, position: "left" | "right") {
  const { name } = fighter;
  const container = createElement({ tagName: 'div', className: 'arena___fighter-indicator' });
  const fighterName = createElement({ tagName: 'span', className: 'arena___fighter-name' });
  const indicator = createElement({ tagName: 'div', className: 'arena___health-indicator' });
  const bar = createElement({ tagName: 'div', className: 'arena___health-bar', attributes: { id: `${position}-fighter-indicator` }});

  fighterName.innerText = name;
  indicator.append(bar);
  container.append(fighterName, indicator);

  return container;
}

function createFighters(firstFighter: Fighter, secondFighter: Fighter) {
  const battleField = createElement({ tagName: 'div', className: `arena___battlefield` });
  const firstFighterElement = createFighter(firstFighter, 'left');
  const secondFighterElement = createFighter(secondFighter, 'right');

  battleField.append(firstFighterElement, secondFighterElement);
  return battleField;
}

function createFighter(fighter: Fighter, position: "left" | "right") {
  const imgElement = createFighterImage(fighter);
  const positionClassName = position === 'right' ? 'arena___right-fighter' : 'arena___left-fighter';
  const fighterElement = createElement({
    tagName: 'div',
    className: `arena___fighter ${positionClassName}`,
  });

  fighterElement.append(imgElement);
  return fighterElement;
}
