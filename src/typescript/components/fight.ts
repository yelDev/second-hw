import { Fighter } from "../../types/index";
import { FIGHTER_CONTROLS, FIRST_FIGHTER_COMBINATION, SECOND_FIGHTER_COMBINATION } from "../../constants/constants";

export async function fight(firstFighter: Fighter, secondFighter: Fighter): Promise<any> {
  const firstFighterHealthBar = document.getElementById('left-fighter-indicator');
  const secondFighterHealthBar = document.getElementById('right-fighter-indicator');

  return new Promise((resolve) => {
    let {
      health: firstFighterHealth,
      attack: firstFighterAttack
    } = firstFighter;
    let {
      health: secondFighterHealth,
      attack: secondFighterAttack
    } = secondFighter;
    
    let playerOneBlocked = false;
    let playerTwoBlocked = false;

    let firstPlayerCombinationActive = true;
    let secondPlayerCombinationActive = true;

    let combinationSequence: Array<string> = [];

    const firstFighterCombination = Object.keys(FIRST_FIGHTER_COMBINATION).filter(item => (item !== "0" && !Number(item)));
    const secondFighterCombination = Object.keys(SECOND_FIGHTER_COMBINATION).filter(item => (item !== "0" && !Number(item)));

    const resolveFight = (winner: Fighter) => {
      document.removeEventListener('keypress', keyPressHandler);
      document.removeEventListener('keydown', keyDownHandler);
      document.removeEventListener('keyup', keyUpHandler);
      resolve(winner);
    };

    function keyPressHandler (e: { code: any; }) {
      switch (e.code) {
        case FIGHTER_CONTROLS.PlayerOneBlock:
          playerOneBlocked = true;
          break;
        case FIGHTER_CONTROLS.PlayerTwoBlock:
          playerTwoBlocked = true;
          break;
        default:
          console.log("another keypress");
      }
    }

    document.addEventListener('keypress', keyPressHandler);

    function keyUpHandler (e: { code: string; }) {
      let combination: string | string[] = e.code;
      combinationSequence.length === 3 
        && firstFighterCombination.sort().every(function(value, index) { return value === combinationSequence.sort()[index]}) ?
        combination = firstFighterCombination
        : secondFighterCombination.sort().every(function(value, index) { return value === combinationSequence.sort()[index]}) 
        ? combination = secondFighterCombination : null;

      switch(combination) {
        case FIGHTER_CONTROLS.PlayerOneAttack:
          if (!playerOneBlocked && !playerTwoBlocked) {
            secondFighterHealth -= getDamage(firstFighter, secondFighter);
            showCurrentHealth(secondFighterHealth, secondFighter.health, secondFighterHealthBar);
          }
          break;
        case FIGHTER_CONTROLS.PlayerTwoAttack:
          if (!playerTwoBlocked && !playerOneBlocked) {
            firstFighterHealth -= getDamage(secondFighter, firstFighter);
            showCurrentHealth(firstFighterHealth, firstFighter.health, firstFighterHealthBar);
          }
          break;
          case firstFighterCombination:
            if (firstPlayerCombinationActive) {
              secondFighterHealth -= firstFighterAttack * 2
              showCurrentHealth(secondFighterHealth, secondFighter.health, secondFighterHealthBar);
              firstPlayerCombinationActive = false;
            }
            setTimeout(() => firstPlayerCombinationActive = true, 10000)
            break;
        case secondFighterCombination:
          if (secondPlayerCombinationActive) {
            firstFighterHealth -= secondFighterAttack * 2
            showCurrentHealth(firstFighterHealth, firstFighter.health, firstFighterHealthBar);
            secondPlayerCombinationActive = false;
          }
          setTimeout(() => secondPlayerCombinationActive = true, 10000)
          break;
        case FIGHTER_CONTROLS.PlayerOneBlock:
          playerOneBlocked = false;
          break;
        case FIGHTER_CONTROLS.PlayerTwoBlock:
          playerTwoBlocked = false;
          break;
        default:
          console.log("another keydown");
      }
      combinationSequence.splice(0, combinationSequence.length);
      if (firstFighterHealth <= 0) {
        resolveFight({...secondFighter, health: secondFighterHealth});
      }
      if (secondFighterHealth <= 0) {
        resolveFight({...firstFighter, health: firstFighterHealth});
      }
    }

    document.addEventListener('keyup', keyUpHandler);

    function keyDownHandler (e: { code: string; }) {
      if (firstFighterCombination.includes(e.code) || secondFighterCombination.includes(e.code)) {
        combinationSequence.push(e.code);
      }
    }

    document.addEventListener("keydown", keyDownHandler);

  });
}

export function getDamage(attacker: Fighter, defender: Fighter): number {
  let attackPower = getHitPower(attacker);
  let blockPower = getBlockPower(defender);
  let damage = attackPower - blockPower;
  return damage < 0 ? 0 : damage;
}

export function getHitPower(fighter: Fighter): number {
  let power = null;
  power = fighter.attack * (Math.random() + 1);
  return power;
}

export function getBlockPower(fighter: Fighter): number {
  let block = null;
  block =  fighter.defense * (Math.random() + 1);
  return block;
}

function showCurrentHealth(current: number, basic: number, el: HTMLElement): number {
  const result  = current / basic * 100;
  result > 0 ?
    el.style.width = `${result}%` :
    el.style.width = `0%`;
  return result;
}