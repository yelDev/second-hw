import { showModal } from "./modal";
import { createElement } from "../../helpers/domHelper";
import { createFighterImage, createFighterInfo } from "../fighterPreview";
import { Fighter } from "../../../types/index";

function onClose(): void {
  window.location.reload();
};

export function showWinnerModal(fighter: Fighter): void {
  let data: Array<HTMLElement> = [];

  const bodyElement = createElement({
    tagName: "div",
    className: "modal-body"
  });

  const winnerImg = createFighterImage(fighter);
  const infoContainer = createFighterInfo(fighter);

  data = [winnerImg, infoContainer];
  bodyElement.append(...data);

  showModal({
    title: `Winner: ${fighter.name}`,
    bodyElement,
    onClose
  });

}
