import { Element } from "../../types/index";

export function createElement({ tagName, className, attributes = {} }: Element): HTMLElement {
  const element = document.createElement(tagName);

  if (className) {
    const classNames: Array<string> = className.split(' ').filter(Boolean);
    element.classList.add(...classNames);
  }

  Object.keys(attributes).forEach((key) => element.setAttribute(key, attributes[key]));

  return element;
}
