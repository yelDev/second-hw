import { IAttributes } from "../interfaces/index";

export type Fighter = { 
  _id?: string, 
  name?: string, 
  source?: string, 
  health?: number, 
  attack?: number, 
  defense?: number 
};

export type Element = {
  tagName: string,
  className?: string,
  attributes?: IAttributes
}