const presets = [
  [
    "@babel/preset-env",
    {
      targets: {
        firefox: "60",
        chrome: "67"
      }
    }
  ],
  "@babel/preset-typescript"
];

const plugins = [
  [
  "@babel/plugin-transform-runtime",
  {
    "absoluteRuntime": false,
    "corejs": false,
    "helpers": true,
    "regenerator": true,
    "useESModules": false,
    "version": "7.0.0-beta.0"
  }
],
"@babel/proposal-class-properties",
"@babel/proposal-object-rest-spread"
];
  
module.exports = { 
  presets, 
  plugins 
};